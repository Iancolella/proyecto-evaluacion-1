#include <stdio.h>
#include <stdlib.h>
//Declaracion de variables globales

float TEMPERATURA[] = {27.3,27.6,26.3,28.4,27.1,27.9,28.8};
float TENSION[] = {5.1,5.2,5.1,5.5,5.4,6.1,6.6};
float gradosFARENHEIT[] = {};
float promedioTENSION = 0;
float mayorTENSION = 0;

//Bubble sort

void ORDENAR(float a[]){
	int i,j;
    for(j = 0; j<7; j++)
    {
        int cambio = 0;
        i = 0;
        while(i<7-1)
        {
            if (a[i] > a[i+1])
            {
                float temp = a[i];
                a[i] = a[i+1];
                a[i+1] = temp;
                cambio = 1;
            }
            i++;
        }
        if (!cambio)
            break;
    }
    printf("El vector ordenado es igual a: \n");
    for(i=0;i<7;i++)
        printf("%f\n",a[i]);
}

//Conversion de C� a F�

void CONVERSION(){
    int i = 0;
    printf("Los valores en farenheit equivalen a: \n");
    for (i=0;i < (sizeof (TEMPERATURA) /sizeof (TEMPERATURA[0]));i++) {
    printf("C: %f --> F: ",TEMPERATURA[i]);
    gradosFARENHEIT[i] = (TEMPERATURA[i] * 1.8) + 32;
    printf("%f\n",gradosFARENHEIT[i]);
    }
}

//Promedio de las tensiones

void PROMEDIO(){
    int i = 0;
    promedioTENSION = 0;
    for (i=0;i < (sizeof (TENSION) /sizeof (TENSION[0]));i++) {
    promedioTENSION = promedioTENSION + TENSION[i];
    }
    promedioTENSION = promedioTENSION/i;
    printf("El promedio de la tension es igual a :%f \n",promedioTENSION);
}

//Main

int main(){
    int i = 0;
    CONVERSION();
    PROMEDIO();
    ORDENAR(TEMPERATURA);
    ORDENAR(TENSION);
    return 0;
}
